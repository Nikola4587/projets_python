"""Special methodes"""

# Edit the object
class Example:
    """A small example of a class"""
    def __init__(self, name):
        "Example of consructor"
        self.name = name
        self.other_attribute = "value"

my_object = Example("first example")


class Person:
    """Class which defines one person caracterised by :
    - name
    - last name
    """

    def __init__(self, name, last_name):  # methode constructor
        self.name = name
        self.last_name = last_name
        self.age = 15

    def __repr__(self):
        "When we go to object in the interpreter"
        return f"Person : {self.name} {self.last_name} {self.age}"


p1 = Person("Jean", "Luc")
print(f"If we write p1 we obtain -> {p1}")
print(f"If we write repr(p1) we obtain - > {repr(p1)}")

"""How to acces to attributs and modify them"""
class Protect:
    """Class who have the special methode to acces to attributs"""
    def __init__(self):
        self.a = 1
        self.b = 2
        self.c = 3

    def __getattr__(self, item):
        """If Python can not find the attribut he will call this method"""
        print(f"Attention. There is no attribut {item} here")

    def __delattr__(self, name_of_attribut):
        """We can not delete attribut,, we can raise the exception"""
        raise AttributeError ("You can not delete the attribut of cette class")

pro = Protect()
print(f"pro.a is {pro.a}")
print(f"pro.b is {pro.b}")
print(f"pro.e is {pro.e}")
# pro.__delattr__("a")
# print(f"pro.a is {pro.a}")

"""Methodes of contenner"""

class ZDict:
    """Classe enveloppe of dictionnary"""

    def __init__(self):
        self._dictionnary = {}

    def __getitem__(self, key):
        """This methodes is called when we write object[index]"""
        return self._dictionnary[key]

    def __setitem__(self, key, value):
        """This method is called when we write object[key]"""
        self._dictionnary[key] = value

object = ZDict()
object["key"] = "hello"
print(object["key"])