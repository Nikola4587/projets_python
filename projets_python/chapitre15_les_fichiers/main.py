"""The files"""
import os

os.chdir("C:/Users/dimit/solutions/projets_python/chapitre15_les_fichiers")
actual_repository = format(os.getcwd())
print(f"My actual repository is : {actual_repository}")

my_files = open("my_file.txt", "r")  # r mean we want to read the file
my_file_type = type(my_files)
print(f"Here is a typ of my file {my_file_type}")
# the function open send a object of the class TextIoWrapper

my_files_contents = my_files.read() # read the file
print(f"Here is the contents of my file : {my_files_contents}")
my_files.close() # close the file. It is important to do

my_files_write = open("my_file.txt", "w")  # we erase all the contents
my_files_write.write("First test of writing with a Python")
my_files_write.close()

# we can use a key with : the file will be close automaticly
# with will create the context manager and will verify that the file is
# open or closed if we have the errors or not

with open("my_file.txt", "r") as my_file_written:
    text = my_file_written.read()
    print(f"Here is my file written -> {text}")

"""-----------------------------------------------------"""
"Module Path Lib"
from pathlib import Path
chemin = Path.cwd()
print(f"Path lib. There is the directory -> {chemin}")

repository_content = chemin.iterdir() # show what we have in the repository
print(f"Path lib : Here is the repository content -> {repository_content}")

repository_content_listed = list(chemin.iterdir())
print(f"Path Lib : Repository listed -> {repository_content_listed}")


"""----------------------------------------------------------------"""
"""Save the objects in the files"""
# with module pickle, we can save object in the file
import pickle

# save the object in the file datas
with open("datas", "wb") as file_pickle:
    my_pickler = pickle.Pickler(file_pickle)

score = {
    "Player 1": 5,
    "Player 2": 5,
    "Player 3": 5,
    "Player 4": 5,
    "Player 5": 5,
    "Player 6": 5,
}

with open("datas", "wb") as file_pickle:
    my_pickler = pickle.Pickler(file_pickle)
    my_pickler.dump(score)

# Take out saved datas
with open("datas", "rb") as file_pickle:
    my_deplicker = pickle.Unpickler(file_pickle)
    scopre_taken = my_deplicker.load()
    print(f"Pickler : HEre is the score taken -Y {scopre_taken}")