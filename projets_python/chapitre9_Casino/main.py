from random import randint
from math import ceil

print("The game will start, please be ready")

try:
    player_number = int(input("Please chose your number between 0 and 49"))
    money_invested = int(input("How much money do you want to invest ?"))

    while player_number <= 0 or player_number > 49:
        raise ValueError

except ValueError:
    print("Please chose the value between 0 and 49")

random_number = randint(0, 49)

if random_number == player_number:
    gain = money_invested * 3

if random_number % 2 == player_number % 2:  # same color
    gain = ceil(50/100 * money_invested)
else:
    gain = 0
    print("Sorry, you didnt win")
