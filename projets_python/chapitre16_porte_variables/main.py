"""Variables defined in the body of the function"""
def set_var(new_value):
    try:
        print(f"Before affectation, the variable is {var0}")
    except NameError:
        print(f"La variable doesnt exist yet")

    variable = new_value
    print(f"After affectation, the variable is {variable}")


"""The funciton which modify the objects"""
def function_add(list, value_to_add):
    list.append(value_to_add)
    print(f"Here is my list{list}")


"""References (is the position in the memory)"""
# How to modify a list without touching the other
my_list_1 = [1, 2, 3]
my_list_2 = list(my_list_1) # copie
my_list_1.append(4)
print(f"My list 1 {my_list_1}")
print(f"My list 2 {my_list_2}")

if __name__ == '__main__':
    # set_var(5)

    my_list = ["a", "e", "i", "o"]
    function_add(my_list,"r")

