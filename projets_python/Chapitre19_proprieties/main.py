"""First example"""
class Person:
    """Class which defines one person caracterised by :
    - name
    - last name
    """

    def __init__(self, name, last_name):  # methode constructor
        self.name = "Nikola"
        self.last_name = "Dimitrijevic"

    @property
    def complet_name(self):
        return f"{self.name} {self.last_name}"


Nikola = Person(name="Nikola", last_name="Dimitrijevic")
print(f"Example 1 -> Complet name is {Nikola.complet_name}")

"""Second example"""
class Basket:
    """Class who represent a basket shop"""

    def __init__(self):
        self.products = {}

    @property
    def total(self):
        """Return the total price of basket"""
        total = 0
        for products, price in self.products.items():
            total = total + price

        return total

    def buy(self, product_name, price):
        self.products[product_name] = price

basket = Basket()
basket.buy("pencil", 1.5)
basket.buy("paper", 2.0)
print(f"Example 2 -> Basket products are {basket.products}")
print(f"Example 2 -> Total is {basket.total}")


"""Propriety in read and write"""
class Person_read_write:
    """Class which defines one person caracterised by :
    - name
    - last name
    """

    def __init__(self, name, last_name, town):  # methode constructor
        self.name = "Nikola"
        self.last_name = "Dimitrijevic"
        self.town= "Crissier"

    @property
    def complet_name(self):
        return f"{self.name} {self.last_name}"

    @complet_name.setter
    def complet_name(self, new_name):
        try:
            name, last_name = new_name.split(" ")
        except ValueError:
            raise ValueError("precise un name with a space")
        else:
            self.name = name
            self.last_name = last_name

    @property
    def home_town(self):
        return self.town

    @home_town.setter
    def home_town(self, new_town):
        self.town = new_town

person = Person_read_write(name="Ana", last_name="Bekuta",town="Paris" )
person.complet_name = "Sejo Kalac"
person.town="Uzici"
print(f"Name of person is {person.name}")
print(f"Last name of person is {person.last_name}")
print(f"New town of person is {person.town}")