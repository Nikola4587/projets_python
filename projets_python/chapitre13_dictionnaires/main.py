"""Dictionnaires are the objects that have others.
They are maching each object to a key - string in the most of the time"""

# To crate a dictionary : my_dictionary = dict() or dict = {}
# The key of dictionnary : my_dict["key"] = "value"
my_dic = {}
my_dic["name"] = "Nikola"
my_dic["password"] = "Sevojno"
print(f"Here is my dictionary{my_dic}")

for key in my_dic.keys():
    print(f"There are the keys {key}")

for value in my_dic.values():
    print(f"There are the values {value}")

"""-----------------------------------------"""
# To print in the same time keys and values, we are using the ITEMS
for keey, vallues in my_dic.items():
    print(f"There key {keey} has the value {vallues}")

"""Take the named parameters in the dictionnary"""
def unknown_function(**named_parametres):
    print(f"I received {named_parametres}")

unknown_function(p=4, j=8)
# To capture all parametres named to a dict, we need to add double *

"""IF WE WANT THE ONE FUCNTION ACCEPT ALL THE PARAMETRES NAMED OR NOT,
WE NEED TO DECLARE LIKE THIS """
#def function_accept_all_arguments (*in_liste, **in_dictionnary):
    # all the parametres non named will be in liste
    # all the parametres named will be in_dictionnary

