"""List can have different objects as int, float, string or a mix of them"""


"""Create a list  my_list = list() or my_list = []"""

"""" How to accede to the list elements"""
my_list = ["c", "f", "m"]
# Take the first element of a list
print("The first object of my list is {}".format(my_list[0]))

# Replace the elements of a list
my_list[0] = "M"
print(f"Word c has been changed by M : {my_list}")

# Add elements to a list
my_list.append("O")
print(f"Add a lettre O in the end of a list {my_list}")

# Concatenate elements of the list
list_1 = [1, 2, 3]
list_2 = [4, 5, 6]
list_1.extend(list_2)

print(f"List extended : {list_1}")

"""Remove the elements from a list"""
# the key is del
# the methode is remove

del list_1[0]
print(f"Remove the first number of a list using a key del {list_1}")

list_1.remove(2)
print(f"Remove the second number of a list using a methode remove {list_1}")

"""The function enumerate. This funciton takes the indice and element in 2
differents variables """
my_character_list = ["a", "b", "c", "d", "e", "f"]
for indice, letter in enumerate(my_character_list):
    print(f"At indice {indice} is letter {letter}")




"""Tuples. The tuples is a list which we can not modify"""

"""Create a tuple  my_tuple = ()"""
(a,b) = (3, 4)

def decompose(numerator, denominator):
    value_int = numerator // denominator
    reste = numerator % denominator

    return value_int, reste

value_int, reste = decompose(20,3)
print(f"Value entier is {value_int} and the reste is {reste}")


