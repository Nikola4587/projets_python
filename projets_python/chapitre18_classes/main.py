# 1. Objet : variables qui contiennent des autres variables
# 2. Methodes : function des objets, actions
# 3. Attribus : variable propre a objet
# 4. Constructeur : est appelle quand on veut creer un objet depuis notre class

class Person:
    """Class which defines one person caracterised by :
    - name
    - last name
    - age
    - town
    """

    def __init__(self):  # methode constructor (when we call the function this code will be run
        self.name = "Nikola"
        self.last_name = "Dimitrijevic"
        self.age = "24"
        self.town = "Crissier"


if __name__ == '__main__':
    personne = Person()

    print(f"Person is {personne.town}")

# Methodes
class Blackboard:
    def __init__(self):  # constructor
        """By default, our surface is empty"""
        self.surface = ""

    def write_blackboard(self, message_to_write):
        """Methode to write a message on the blackboard"""
        if self.surface != "":
            self.surface += "\n"
        self.surface = self.surface + message_to_write

    def read_blackboard(self):
        """This method will read our blackboard """
        print(f"On blackboard is written :{self.surface}")

    def delete_blackboard(self):
        """This method will clean the blackboard"""
        self.surface = ""
blackboard = Blackboard()
blackboard.write_blackboard("Coool")
blackboard.read_blackboard()
blackboard.delete_blackboard()
blackboard.read_blackboard()


# Parameter self is the objec which call a method


# Special attribut __dict__
# special attributs are using __XXX__
"""Dictionnary who has the names and attributs assosiated to his values"""
print(blackboard.__dict__)
