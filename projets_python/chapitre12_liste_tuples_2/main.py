# Second part of the lists and tuples

"""Convert the chain to a list (command split)"""
my_chain = "Hello everybody"
my_chain_splitted = my_chain.split(" ")
print(f"There is my chain {my_chain}")
print(f"My chain splitted{my_chain_splitted}")
"""-------------------------------------------------------------"""

"""Convert a list to the chain"""
my_list = ['Hy', 'to', 'everybody']
#print(f"There is my list {my_list}")
" ".join(my_list)
print(f"My list joined {my_list}")
"""-------------------------------------------------------------"""

"""Application of these functionalities"""
def afficher_flottant(number_float):
    if type(number_float) is not float:
        raise TypeError("Please enter a float")
    number_float = str(number_float)
    int_part, float_part = number_float.split(".")
    print(int_part + "," + float_part[0:3])


afficher_flottant(3.99999999988989898)

"""-------------------------------------------------------------"""

"""Function which we don't know the number of parameters in advance
 * is used before the name which will have the list of arguments. """
def my_unknown_function(*my_function_parametres):
    print(f"The function has reveived : {my_function_parametres}")

my_unknown_function()
my_unknown_function(33)
my_unknown_function("a", "e")
"""All he parameters are placed in the tuple, which we can use after
We can also have 2 parametres that should be known and one list of variable parametres
example : my_unknown_function(name, last_name, *my_function_parametres)"""

"""-------------------------------------------------------------"""

"""List comprehensions : the way to filter or modify a list"""
origin_list = [0, 1, 2, 3, 4, 5]
print(f"List comprehensions : here is my origin list:{origin_list}")
modified_list = [number * number for number in origin_list]
print(f"Here is the list multiplied{modified_list}")

"""Print only the numbers that we can divide by 2"""
divided_by_2_numbers = [number for number in origin_list if number % 2 == 0]
print(f"Here is a list of numbers divided by 2 : {divided_by_2_numbers}")

"""-------------------------------------------------------------"""
"""Small exercice"""
quantity_of_fruits_to_remove_each_week = 7
fruits_in_stock = [15, 3, 18, 21] # quantity of each fruit
list_final = [fruit - quantity_of_fruits_to_remove_each_week for fruit in fruits_in_stock if fruit > quantity_of_fruits_to_remove_each_week]
print(list_final)

"""-------------------------------------------------------------"""
"""Named tuples"""
inventory = [
    ("pommes", 22),
    ("melons", 4),
    ("poires", 18),
    ("fraises", 76),
    ("prunes", 51),
]
# Change de sens on fruit and quantity
separate_fruit_quantity = [(quantity, fruit) for fruit, quantity in inventory]

# Sorted from higher to lower quantity
sorted_invntory = sorted(separate_fruit_quantity,reverse=True)

# Reverse to have first  fruit and then quantity
inverse_inventory = [(fruit_final, quantity_final) for quantity_final, fruit_final in sorted_invntory ]
print(inverse_inventory)